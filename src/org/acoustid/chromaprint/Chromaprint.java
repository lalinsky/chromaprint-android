package org.acoustid.chromaprint;

public class Chromaprint {

    private final long ctx = 0;

    public Chromaprint() {
        create();
    }

    protected void finalize() {
        destroy();
    }

    private native void create();

    private native void destroy();

    public native void start(int sampleRate, int channels);

    public native void feed(short[] samples);

    public native void finish();

    public native String getFingerprint();

    public native int[] getRawFingerprint();

    static {
        System.loadLibrary("chromaprint-jni");
    }

}
