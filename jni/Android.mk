LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

MY_CHROMAPRINT_SRC_FILES := \
    ../deps/chromaprint/src/simhash.cpp \
    ../deps/chromaprint/src/audio_processor.cpp \
    ../deps/chromaprint/src/base64.cpp \
    ../deps/chromaprint/src/chroma.cpp \
    ../deps/chromaprint/src/chroma_filter.cpp \
    ../deps/chromaprint/src/chromaprint.cpp \
    ../deps/chromaprint/src/chroma_resampler.cpp \
    ../deps/chromaprint/src/fft.cpp \
    ../deps/chromaprint/src/fft_lib_kissfft.cpp \
    ../deps/chromaprint/src/filter.cpp \
    ../deps/chromaprint/src/fingerprint_calculator.cpp \
    ../deps/chromaprint/src/fingerprint_compressor.cpp \
    ../deps/chromaprint/src/fingerprint_decompressor.cpp \
    ../deps/chromaprint/src/fingerprinter_configuration.cpp \
    ../deps/chromaprint/src/fingerprinter.cpp \
    ../deps/chromaprint/src/image_builder.cpp \
    ../deps/chromaprint/src/integral_image.cpp \
    ../deps/chromaprint/src/silence_remover.cpp \
    ../deps/chromaprint/src/spectral_centroid.cpp \
    ../deps/chromaprint/src/spectrum.cpp \
    ../deps/chromaprint/src/avresample/resample2.c

MY_KISSFFT_SRC_FILES := \
    ../deps/kissfft/kiss_fft.c \
    ../deps/kissfft/tools/kiss_fftr.c

LOCAL_MODULE := chromaprint-jni
LOCAL_SRC_FILES := chromaprint-jni.c $(MY_CHROMAPRINT_SRC_FILES) $(MY_KISSFFT_SRC_FILES)
LOCAL_CFLAGS := -DWITH_KISSFFT -DHAVE_ROUND
LOCAL_C_INCLUDES := $(LOCAL_PATH)/../deps/chromaprint/src $(LOCAL_PATH)/../deps/kissfft

include $(BUILD_SHARED_LIBRARY)
